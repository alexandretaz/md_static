/**
 * Created with JetBrains PhpStorm.
 * User: alexandretaz
 * Date: 04/07/13
 * Time: 00:42
 * To change this template use File | Settings | File Templates.
 */

 var corpo = {
    hideInfoDivs : function(){$('.inactiveOverlay').hide();},
    getDiveBasedInTheLink : function(objeto)
    {

      if(objeto != undefined)
      {
        var idToChange = $(objeto).attr('id');

        if(idToChange != undefined){
          var newString = idToChange.replace("info_", "div_");
          return newString;
        }
          alert("A informação desejada não foi encontrada!");
      }

    },
    getShowInfo: function(objeto){
        divToGet = corpo.getDiveBasedInTheLink(objeto);
       if($("#"+divToGet) !==undefined){
           $("#"+divToGet).dialog({modal:true, width:"500"});
       }
       else{
           alert("A informação desejada não foi encontrada!");
       }
    }
 }
$(document).ready( function(){
    corpo.hideInfoDivs();
    $('.linkInfo').click (function(){corpo.getShowInfo(this)});
});
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35852941-3', 'maniacdivers.com.br');
  ga('send', 'pageview');